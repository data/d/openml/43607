# OpenML dataset: WMO-Hurricane-Survival-Dataset

https://www.openml.org/d/43607

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

It is estimated that 10,000 people die each year worldwide due to hurricanes and tropical storms. The
majority of human deaths are caused by flooding.
Hurricane Irma hit Florida as a Category 4 storm the morning of Sept. 10, 2017, ripping off roofs,
flooding coastal cities, and knocking out power to more than people. The storm and its aftermath has
killed at least 38 in the Caribbean, 34 in Florida, three in Georgia, four in South Carolina, and one in
North Carolina.
The occurrences of these natural disasters have been on a high which is a concern for United Nation;
The World Meteorological Organization (specialized agency of UN) has been collecting data about all
the individuals that are living in and around Hurricanes and Cyclones prone areas. In the aftermath of
Irma, WMO wants to find a pattern or a relation between the attributes that will prove whether an
individual will SURVIVE OR NOT SURVIVE any hurricane/cyclones in the near future.
DATA DICTIONARY
 VARIABLES    DESCRIPTION
 DOB    Date of Birth(MM/DD/YYYY)
 MSTATUS    Marital Status (Married/Unmarried/Divorced)
 SALARY    Annual salary ( specified in Ranges)
 EDUDATA    Education details ( Uneducated/High-School/Gradute / Post-Graduate)
 EMPDATA    Employment details ( Employed/Self-Employed/unemployed)
 RELORIEN    Religious orientation ( Agnostic / Atheist / Believer)
 FAVTV    Favourite TV Show
 PREFCAR    Preferred brand of car
 GENDER    Gender( Male/Female/Other)
 FAVCUIS    Favourite Cuisine
 FAVMUSIC    Favourite Genre of Music
 ENDULEVEL    Endurance Level
 FAVSPORT    Favourite sport
 FAVCOLR    Favourite color
 NEWSSOURCE    Source of the news
 DISTFRMCOAST    Distance from the coast
 MNTLYTRAVEL    Monthly travel
 GENMOVIES    Preferred Genre of Music
 FAVSUBJ    Favourite subject
 ALCOHOL    Preferred Alcohol
 FAVSUPERHERO    Favourite Superhero
 Class    x(will survive) and y(Will not survive)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43607) of an [OpenML dataset](https://www.openml.org/d/43607). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43607/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43607/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43607/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

